package averageHomeWork;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class AverageHomeWorkMain {
	public static void main(String[] args) {
		HomeWork homeWork = new HomeWork();
		String filenameRead = "homework.txt";
		String filename = "average.txt";
		FileReader fileReader = null;
		FileWriter fileWriter = null;
		
		try {
			//read file
			FileReader fileReader2 = new FileReader(filenameRead);
			BufferedReader buffer = new BufferedReader(fileReader2);			
			
			fileWriter = new FileWriter(filename);
			PrintWriter out = new PrintWriter(fileWriter);	
			
			String line;
			
			for(line = buffer.readLine(); line != null; line=buffer.readLine()){
				String[] data = line.split(",");
				homeWork.readHomeWork(data);
				out.println("\n"+homeWork.getName()+", "+homeWork.getAverageScore());					
			}
			out.flush();
			
			fileReader = new FileReader(filename);
			BufferedReader buffer1 = new BufferedReader(fileReader);	
			String line2;
			
			System.out.println("==========AVERAGE HOMEWORK==========\n");
			System.out.println("\tNAME\t|AVERAGE ");
			System.out.println("\t------\t|-------");
			for(line2 = buffer1.readLine(); line2 != null; line2=buffer1.readLine()){
				String[] data2 = line2.split(",");
				
				System.out.println("\t "+data2[0]+"\t| "+data2[1]);					
			}
			System.out.println("\n====================================");
			
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filenameRead);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} 
			catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		
	}
}
