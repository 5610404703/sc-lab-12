package averageHomeWork;

public class HomeWork {
	private String name;
	private double sumScore;
	private double countHomwork;
	
	public void readHomeWork(String[] data){
		for (int i = 0; i < data.length; i++) {
			if(i==0){
				this.name = data[0];
			}
			else{
				sumScore += Double.parseDouble(data[i]);			
			}			
		}
		countHomwork = data.length-1;
	}
	
	public String getName(){
		return name;
	}
	public double getAverageScore(){
		return sumScore/countHomwork;
	}
	
	
}
