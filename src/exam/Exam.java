package exam;

public class Exam {
	private String name;
	private String score;
	private double sumScore;
	private double countScore;
	
	public void readExam(String[] data){
		for (int i = 0; i < data.length; i++) {
			if(i==0){
				this.name = data[0];
			}
			else{
				sumScore += Double.parseDouble(data[i]);			
			}			
		}
		countScore = data.length-1;
	}
	
	public String getName(){
		return name;
	}
	
	public String getScore(){
		return score;
	}
	
	public double getAverageScore(){
		return sumScore/countScore;
	}

}
