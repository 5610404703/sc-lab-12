package exam;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class AverageExam {
	public static void main(String[] args) {
		Exam exam = new Exam();
		String filename = "exam.txt";
		
		FileReader fileReader = null;
		FileWriter fileWriter = null;
		
		try {
			FileReader fileReader2 = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader2);

			fileReader = new FileReader("average.txt");
			BufferedReader buffer1 = new BufferedReader(fileReader);	
			String line;
			
			fileWriter = new FileWriter("average.txt", true);
			PrintWriter out = new PrintWriter(fileWriter);
			
			System.out.println("==========HOMEWORK SCORE==========\n");
			System.out.println("\tNAME\t|AVERAGE ");
			System.out.println("\t------\t|-------");
			
			for(line = buffer1.readLine(); line != null; line=buffer1.readLine()){
				String[] data2 = line.split(",");	
				System.out.println("\t "+data2[0]+"\t| "+data2[1]);					
			}
			
	
			System.out.println("==========EXAM SCORE==========\n");		
			for(line = buffer.readLine(); line != null; line=buffer.readLine()){
				String[] data = line.split(",");
				exam.readExam(data);
				out.println(exam.getName()+", "+exam.getAverageScore());
				System.out.println("\t"+exam.getName()+"\t| "+exam.getAverageScore());
			}
			
			out.flush();
			System.out.println("\n====================================");
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} 
			catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		
	}
}
