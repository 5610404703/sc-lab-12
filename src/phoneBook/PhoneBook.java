package phoneBook;

import java.util.ArrayList;
import java.util.List;

public class PhoneBook{
	private List<DataPhonNumber> dataPhonNumber = new ArrayList<DataPhonNumber>();
	private DataPhonNumber phoneNumber;
	
	public void readPhoneBook(String[] data){
		String name = data[0].trim();
		String phoneNum = data[1].trim();
		phoneNumber = new DataPhonNumber(name, phoneNum);
		dataPhonNumber.add(phoneNumber);
	}
	
	public List<DataPhonNumber> getDataPhonNumber(){
		return dataPhonNumber;
	}
}