package phoneBook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class PhoneBookMain {

	public static void main(String[] args) {
		PhoneBook phone = new PhoneBook();
		String filename = "phonebook.txt";
		FileReader fileReader = null;
		
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			
			for(line = buffer.readLine(); line != null; line=buffer.readLine()){
				String[] data = line.split(",");
				phone.readPhoneBook(data);									 
			}	
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} 
			catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		
		List<DataPhonNumber> dataNum = phone.getDataPhonNumber();
		System.out.println("==============PHONE BOOK==============\n");
		System.out.println("\tNAME\t|PHON NUMBER ");
		System.out.println("\t-------\t|-------------");
		for(DataPhonNumber d:dataNum){
			System.out.println(" \t"+d.getName()+"\t| "+d.getPhoneNumber());		
		}
		System.out.println("\n======================================");

	}

}